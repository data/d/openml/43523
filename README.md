# OpenML dataset: Yu-Gi-Oh-Normal-Monster-Cards

https://www.openml.org/d/43523

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Yu-Gi-Oh! is a Japanese manga series about gaming written and illustrated by Kazuki Takahashi. It was serialized in Shueisha's Weekly Shnen Jump magazine between September 30, 1996 and March 8, 2004. The plot follows the story of a boy named Yugi Mutou, who solves the ancient Millennium Puzzle. Yugi awakens a gambling alter-ego within his body that solves his conflicts using various games.
Two anime adaptations were produced; one by Toei Animation, which aired from April 4, 1998 to October 10, 1998,[2] and another produced by NAS and animated by Studio Gallop titled Yu-Gi-Oh! Duel Monsters, which aired between April 2000 and September 2004. The manga series has spawned a media franchise that includes multiple spin-off manga and anime series, a trading card game, and numerous video games. Most of the incarnations of the franchise involve the fictional trading card game known as Duel Monsters, where each player uses cards to "duel" each other in a mock battle of fantasy "monsters". This forms the basis for the real life Yu-Gi-Oh! Trading Card Game. As of 2018, Yu-Gi-Oh is one of the highest-grossing media franchises of all time.
PLEASE UPVOTE

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43523) of an [OpenML dataset](https://www.openml.org/d/43523). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43523/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43523/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43523/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

